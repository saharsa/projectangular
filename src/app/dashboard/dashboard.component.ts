import { style } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  Highcharts = Highcharts;
  allIncident: {};
  allrental: {};
  allIncidentResolved: {};
  allIncidentCategory: {};
  lostCasePerMonth: {};
  
  constructor() { }


  // Line chart that show trend rental 
  loadallrental(){
    this.allrental = {
    title:{
        text: null
    },
    chart: {
        height: 250,
        plotAreaHeight: 300,
        backgroundColor: "rgba(129, 211, 248, 0.5)"
    }, 
    plotOptions: {
        series: {
            color: '#ffcc00'
        }
    },

    yAxis: {
        title: {
            enabled: false,
            text: 'Custom with <b>simple</b> <i>markup</i>',
        }
    },

    xAxis: {
        categories: ['Sep', 'Oct', 'Nov', 'Dec','Jan', 'Feb',],
        labels: {
            rotation: 30
        }
    },

    series: [{
        name: 'rental',
        data: [222356, 385912, 285155, 321533, 288798, 377575]
    }],
    legend: {
        enabled: false,
    },
    }
  }

  incidentresolved(){
    this.allIncidentResolved = {
    title:{
        text: 'Percentage of incidants resolved from all incidents',
        align: 'left',
    },
    subtitle: {
        text: 'Target: 70% per week',
        verticalAlign: 'bottom',
        align: 'left',
    },
    chart: {
        height: 250,
        plotAreaHeight: 300,
        backgroundColor: "rgba(129, 211, 248, 0.5)"
    }, 
    plotOptions: {
        series: {
            color: '#ffcc00'
        }
    },
    yAxis: {
        title: {
            enabled: false,
            text: 'Custom with <b>simple</b> <i>markup</i>',
        },
        labels: {
            format: '{value}%'
          }
    },

    xAxis: {
        categories: ['Week 1', 'Week 2', 'Week 3', 'Week 4','Week 4', 'This week',],
        labels: {
            rotation: 30
        }
    },

    series: [{
        name: 'incident resolved',
        data: [61, 31, 70, 78, 67, 53]
    }],
    legend: {
        enabled: false,
    },
    tooltip: {
        formatter: function () {
            return 'The incidents resolved <br> in <b>' + this.x +
                '</b> are <b>' + this.y + '%</b>';
        }
    },
    }
  }

  incidentByCategory(){
    this.allIncidentCategory = {
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Incidents by category'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                    distance: -30,
                    filter: {
                        property: 'percentage',
                        operator: '>',
                        value: 4
                    }
                }
            }
        },
        series: [{
            name: 'Incident',
            data: [
                { name: 'Repairs', y: 30 },
                { name: 'Payment', y: 18 },
                { name: 'Maintenance', y: 45 },
                { name: 'Electrictiy', y: 7 },
            ]
        }]
    }
  }

  lostcase(){
    this.lostCasePerMonth = {
        chart: {
            type: 'pie',
        },
        title: {
            text: 'Number of Total-lost <br> cases this month'
        },
        subtitle: {
            text: 'Last month <b>5</b> cases',
            verticalAlign: 'bottom',
            style: {
                fontSize: '16px'
            }
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.y}</b><br>',
                    distance: -75,
                    style: {
                        color: '#FFFFFF',
                        fontSize: '50px'
                    }
                }
            }
        },
        series: [{
            name: 'Num',
            data: [
                { name: 'lost case', y: 7 ,color: '#8B0000'},
            ],
        }]
    }
  }

  ngOnInit(): void {
    this.loadallrental();
    this.incidentresolved();
    this.incidentByCategory();
    this.lostcase();
  }


}
