import { AuthService } from './../services/auth.service';
import { IncidentService } from './../services/incident.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-incident-form',
  templateUrl: './incident-form.component.html',
  styleUrls: ['./incident-form.component.css']
})
export class IncidentFormComponent implements OnInit {

  subject;
  text;
  category;
  userId;
  constructor(private incidentsrv:IncidentService,public auth:AuthService,private router:Router) { }

  onSubmit(){
    this.incidentsrv.classify(this.text).subscribe(
      res => {
        this.category = this.incidentsrv.categories[res]
        console.log('this category:',this.category)
        this.incidentsrv.addincident(this.userId,this.subject,this.category);
        this.router.navigate(['/allIncidents']);
      }
    )
  }

  ngOnInit(): void {
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
      }
    )
  }

}
