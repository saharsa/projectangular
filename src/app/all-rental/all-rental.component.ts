import { RentingService } from './../services/renting.service';
import { IncidentService } from './../services/incident.service';
import { AuthService } from './../services/auth.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { Observable, of } from 'rxjs';
import { DataSource } from '@angular/cdk/table';

@Component({
  selector: 'app-all-rental',
  templateUrl: './all-rental.component.html',
  styleUrls: ['./all-rental.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AllRentalComponent implements OnInit {

  displayedColumns = ['startDate', 'endDate','total_Amount','numberOfBicycles','edit'];
  arr;
  dataSource;
  userId;
   flxdata =[];
   userEmail;
   title:string = 'My Rental';

  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  expandedElement: any;

  constructor(private rentsrv:RentingService, private auth:AuthService) {
    
  }

  delete(id){
    this.rentsrv.deleteRental(this.userId,id);
  }

  ngOnInit(): void {
/**   this.rentsrv.getRents().subscribe(
      (res) => (
        this.arr=res,
        console.log(this.arr),
        this.dataSource = new ExampleDataSource(this.arr)
        
      )
    )
*/ 
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        this.userEmail = user.email;
        this.rentsrv.getRentals(this.userId).subscribe(
          (res) => (
            this.arr = res,
            this.dataSource = new ExampleDataSource(this.arr)
          )
        )
      }
    )
    if(this.flxdata instanceof Array){
      console.log('this is arr')
    }
  }

}

export class ExampleDataSource extends DataSource<any>  {
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  constructor(ss){
    super();
    this.arr=ss;
  }
  arr;
  connect(): Observable<any[]> {
    console.log(this.arr)
    const rows = [];
    this.arr.forEach(element => rows.push(element, { detailRow: true, element }));
    console.log(rows);
    return of(rows);
  }

  disconnect() { }
}
