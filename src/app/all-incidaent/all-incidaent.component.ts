import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { IncidentService } from '../services/incident.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-all-incidaent',
  templateUrl: './all-incidaent.component.html',
  styleUrls: ['./all-incidaent.component.css']
})
export class AllIncidaentComponent implements OnInit {

  displayedColumns: string[] = ['no','subject', 'category','opendate','status'];
  dataSource = new MatTableDataSource()
  userId;
  userEmail;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  constructor(private srv:IncidentService,private auth:AuthService) {
    
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit(): void {
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        this.userEmail = user.email;
        if(this.userEmail =='a@a.com'){
          this.srv.getAllIncidents().subscribe(
            res =>this.dataSource.data = res
          )
        }else{
          this.srv.getIncidents(this.userId).subscribe(
            res => this.dataSource.data = res,
          )
        }
      }
    )
    // this.srv.getusr().subscribe(
    //   res => this.dataSource.data = res
    // )
    console.log(this.dataSource)
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}
