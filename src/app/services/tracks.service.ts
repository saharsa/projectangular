import { Observable } from 'rxjs';
import { Tracks } from './../interfaces/tracks';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TracksService {

  trackurl="https://data.cityofchicago.org/resource/fg6s-gzvg.json";

  constructor(private http:HttpClient,private db:AngularFirestore,public router:Router) {
    this.images[0] = this.path +'pic1.jpeg' + '?alt=media';
    this.images[1] = this.path +'pic2.jpeg' + '?alt=media';
    this.images[2] = this.path +'pic3.jpeg' + '?alt=media';
    this.images[3] = this.path +'pic4.jpeg' + '?alt=media';
   }

  
  public path:string ="https://firebasestorage.googleapis.com/v0/b/angularpr-a42d5.appspot.com/o/";
  public images:string[] = [];

  getTracks():Observable<any>{
    return this.http.get<Tracks[]>(this.trackurl);
  }
}
