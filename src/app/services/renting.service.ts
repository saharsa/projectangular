import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { DatePipe } from '@angular/common';
import { pipe } from 'rxjs/internal/util/pipe';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class RentingService {
  pipe = new DatePipe('en-US');
  constructor(private db:AngularFirestore) { }

  rentCollection:AngularFirestoreCollection=this.db.collection('rent');
  userCollection:AngularFirestoreCollection=this.db.collection('users');

  getAllRentals(){
    return this.db.collection('AllRentals').valueChanges()
  }

  getRentals(userId):Observable<any[]>{
    this.rentCollection = this.db.collection(`users/${userId}/rent`);
        return this.rentCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            data.id = a.payload.doc.id;
            return { ...data };
          }))
        ); 
  }


  saveRent(userId:string,startDate:Date, endDate:Date, children_bicycles:number, Road_bicycles:number,Mountain_bicycles:number,Electric_bicycles:number,Helmet:number,Glasses:number, Protectors:number, totalAmount:number){
   
    const start = this.pipe.transform(startDate, 'dd/MM/yyyy');
    const end = this.pipe.transform(endDate, 'dd/MM/yyyy');
    const numberOfBicycles = children_bicycles+Road_bicycles+Mountain_bicycles+Electric_bicycles;
    const rent={numberOfBicycles:numberOfBicycles,startDate:start , endDate:end , children_bicycles:children_bicycles , Road_bicycles:Road_bicycles , Mountain_bicycles:Mountain_bicycles , Electric_bicycles:Electric_bicycles , Helmet:Helmet , Glasses:Glasses , Protectors:Protectors,total_Amount:totalAmount}
   this.rentCollection=this.db.collection(`users/${userId}/rent`);
    this.rentCollection.add(rent);
    this.db.collection('AllRentals').add(rent);
  }

  getrent(userId, id:string):Observable<any>{
    return this.db.doc(`users/${userId}/rent/${id}`).get();
  }

  updateRental(userId:string, id:string,startDate:Date, endDate:Date, children_bicycles:number, Road_bicycles:number,Mountain_bicycles:number,Electric_bicycles:number,Helmet:number,Glasses:number, Protectors:number, totalAmount:number){
    const start = this.pipe.transform(startDate, 'dd/MM/yyyy');
    const end = this.pipe.transform(endDate, 'dd/MM/yyyy');
    const numberOfBicycles = children_bicycles+Road_bicycles+Mountain_bicycles+Electric_bicycles;
    const rental = {
      startDate:start,
      endDate:end,
      numberOfBicycles:numberOfBicycles,
      children_bicycles:children_bicycles,
      Road_bicycles:Road_bicycles,
      Mountain_bicycles:Mountain_bicycles,
      Electric_bicycles:Electric_bicycles,
      Helmet:Helmet,
      Glasses:Glasses,
      Protectors:Protectors,
      total_Amount:totalAmount
    }
    console.log('this is id',id)
    this.db.doc(`users/${userId}/rent/${id}`).update(rental)
    this.db.doc(`AllRentals/${id}`).update(rental);
   }

  deleteRental(userId:string,id:string){
    this.db.doc(`users/${userId}/rent/${id}`).delete();
  }

}
