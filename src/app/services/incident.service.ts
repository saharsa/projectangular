import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IncidentService {
  
  private url = "https://6i7ywrfc5e.execute-api.us-east-1.amazonaws.com/project"

  public categories:object = {0: 'Electricity', 1: 'Maintenance', 2: 'Payment', 3: 'Repairs', 4: 'Totallost'}
    
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  incidentCollection:AngularFirestoreCollection;

  
  getAllIncidents():Observable<any[]>{
    return this.db.collection('allincidents').valueChanges()
  }

  classify(doc:string):Observable<any>{
    let json = {
      "articles":[
        {"text":doc}
      ]
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res =>{
        let final = res.body.replace('[','')
        final = final.replace(']','')
        return final; 
      })
    )
  }

  getIncidents(userId):Observable<any[]>{
    this.incidentCollection = this.db.collection(`users/${userId}/incidents`);
        return this.incidentCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            data.id = a.payload.doc.id;
            return { ...data };
          }))
        ); 
  }

  addincident(userId:string, subject:string,category:string){
    const incident = {no:Math.round(Math.random() * 10000).toString(),subject:subject,category:category,opendate:this.formatdate(),status:'open'};
    this.userCollection.doc(userId).collection('incidents').add(incident);
    this.db.collection('allincidents').add(incident);
  }

  formatdate():string{
    const currentdate = new Date();
    const day = currentdate.getDate();
    const month = currentdate.getMonth()+1;
    const year = currentdate.getFullYear();
    return `${day}-${month}-${year}`;
  }
  constructor(private db:AngularFirestore,private http:HttpClient) { }

}
