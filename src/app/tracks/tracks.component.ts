import { Observable } from 'rxjs';
import { TracksService } from './../services/tracks.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-tracks',
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.css']
})
export class TracksComponent implements OnInit {

  constructor(public tracksService:TracksService, public router:Router, public route:ActivatedRoute, public authService:AuthService) { }
  
  tracksarr: object[] = [{id:1,img:this.tracksService.images[0],name:'Urban trip',level:'Hard',time:'5',km:'12',des:'Cycle through the karst limestone landscapes that have inspired Chinese painters and poets for centuries on this hiking and biking adventure through China with World Expeditions.'},
  {id:2,name:'Pedal the path',img:this.tracksService.images[1],level:'Medium',time:'3.5',km:'8',des:"See Central America's top spots with Intrepid on this exhilarating cycle tour, that will see you pedal along parts of the iconic Pan America Highway through Nicaragua, Costa Rica and Panama"},
  {id:3,name:'Bike the Baltics',img:this.tracksService.images[2],level:'Medium',time:'3',km:'5',des:"Nepal is recognised to be one of the world's great mountain biking destinations and this superb cross-country bike ride with KE Adventure Travel is a great way to experience it. "},
  {id:4,name:'Mountain bike',img:this.tracksService.images[3],level:'Easy',time:'3',km:'6',des:"The Baltic states of Estonia, Latvia, and Lithuania are perfect for biking, offering a beguiling mix of fairy tale towns and wild, untouched nature. This 11-day cycle tour with Intrepid will take you from the medieval spires of Tallinn and the funky bars of Riga to the endless dunes."},
]
  Tracks$:Observable<any>;

  ngOnInit() {
    this.Tracks$ =this.tracksService.getTracks();
    console.log(this.Tracks$)
  }

}
