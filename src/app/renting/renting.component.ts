import { RentingService } from './../services/renting.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { DatePipe } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-renting',
  templateUrl: './renting.component.html',
  styleUrls: ['./renting.component.css']
})
export class RentingComponent implements OnInit {
  
  id:string;
  isedit:boolean = false;
  userId: string;
  startDate: Date;
  endDate: Date;
  dateError: string;
  totalDays: number;
  validDates: boolean = false;
  children_bicycles: number = 0;
  Road_bicycles: number = 0;
  Mountain_bicycles: number = 0;
  Electric_bicycles: number = 0;
  Helmet: number = 0;
  Glasses: number = 0;
  Protectors: number = 0;
  total_amount: number = 0;
  submit: boolean = false;
  pipe = new DatePipe('en-US'); // Use your own locale



  constructor(public router: Router, public route: ActivatedRoute, public authService: AuthService, public rentingService: RentingService) { }
  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        if(this.id){
          this.isedit = true;
          this.rentingService.getrent(this.userId,this.id).subscribe(
            res => {
              this.startDate = this.changeToDate(res.data().startDate);
              this.endDate = this.changeToDate(res.data().endDate);
              // this.checkTotalDays();
              console.log(this.startDate)
              console.log(this.endDate)
              this.children_bicycles = res.data().children_bicycles;
              this.Road_bicycles = res.data().Road_bicycles;
              this.Mountain_bicycles = res.data().Mountain_bicycles;
              this.Electric_bicycles = res.data().Electric_bicycles;
              this.Helmet = res.data().Helmet;
              this.Glasses = res.data().Glasses;
              this.Protectors = res.data().Protectors;
              this.total_amount = res.data().totalAmount;
              this.checkTotalDays();
            }
          )
        }
      })

  }

  onSubmit() {
    console.log(this.startDate)
    if(this.id){
      this.rentingService.updateRental(this.userId,this.id,this.startDate,this.endDate,this.children_bicycles,this.Road_bicycles,this.Mountain_bicycles,this.Electric_bicycles,this.Helmet,this.Glasses,this.Protectors,this.total_amount)
      this.router.navigate(['/allRental']);
    }else{
      this.rentingService.saveRent(this.userId, this.startDate, this.endDate, this.children_bicycles, this.Road_bicycles, this.Mountain_bicycles, this.Electric_bicycles, this.Helmet, this.Glasses, this.Protectors, this.total_amount);
      this.router.navigate(['/Confirm']);
    }
  }

  changeToDate(date){
    const [year, month, day]: string[] = date.split('/');
    const awesomeDate = `${day}/${month}/${year}`
    return new Date(awesomeDate)
  }

//   changeDayToMonthe(date) {
//     var d = new Date(date),
//         day = '' + (d.getMonth() + 1),
//         month = '' + d.getDate(),
//         year = d.getFullYear();
//     if (month.length < 2) 
//         month = '0' + month;
//     if (day.length < 2) 
//         day = '0' + day;

//     const newdate = [year, month, day].join('-');
//     return new Date(newdate);
// }

  validateStartDate() {
    var now = new Date();
    var start = new Date(this.startDate);
    var end = new Date(this.endDate);
    const utc1 = Date.UTC(now.getFullYear(), now.getMonth(), now.getDate());
    const utc2 = Date.UTC(start.getFullYear(), start.getMonth(), start.getDate());
    const utc3 = Date.UTC(end.getFullYear(), end.getMonth(), end.getDate());
    if (utc2 < utc1) {
      this.dateError = "The rent's start date allready passed";
      this.validDates = false;
      this.totalDays = null;
    }
    else if (utc2 && utc3 && utc3 < utc2) {
      this.dateError = "The rent's end date is before the start date";
      this.validDates = false;
      this.totalDays = null;
    }
    else {
      this.dateError = "";
      this.checkTotalDays();
    }


  }
  checkTotalDays() {
    if (this.startDate && this.endDate) {
      var start = new Date(this.startDate);
      var end = new Date(this.endDate);
      const utc1 = Date.UTC(start.getFullYear(), start.getMonth(), start.getDate());
      const utc2 = Date.UTC(end.getFullYear(), end.getMonth(), end.getDate());
      var check = Math.floor((utc2 - utc1) / (1000 * 60 * 60 * 24));

      if (check > 0) {
        this.totalDays = check;
        this.validDates = true;
        this.total_amount=(this.children_bicycles*6+this.Road_bicycles*7+this.Mountain_bicycles*14+this.Electric_bicycles*25)*this.totalDays;
      }
      else {
        this.totalDays = null;
        this.validDates = false;
        this.total_amount=0;
      }
      // for updating the resault in the html component
      var app = app.module('daysApp', []);
      app.controller('daysCntl', function () {
      });

    }
  }
  upBycicleChildren() {

    this.children_bicycles++;
    if (this.totalDays) {
      this.total_amount += 6 * this.totalDays;
    }
    var app = app.module('byChApp', []);
    app.controller('byChCtrl', function ($scope) {
    });

  }
  downBycicleChildren() {
    if (this.children_bicycles > 0) {
      this.children_bicycles--;
      if (this.totalDays) {
        this.total_amount -= 6 * this.totalDays;
      }
      var app = app.module('byChApp', []);
      app.controller('byChCtrl', function ($scope) {
      });
    }
  }

  upRoad_bicycles() {
    this.Road_bicycles++;
    if (this.totalDays) {
      this.total_amount += 7 * this.totalDays;
    }
    var app = app.module('byChApp', []);
    app.controller('byChCtrl', function ($scope) {
    });
  }
  downRoad_bicycles() {
    if (this.Road_bicycles > 0) {
      this.Road_bicycles--;
      if (this.totalDays) {
        this.total_amount -= 7 * this.totalDays;
      }
      var app = app.module('byChApp', []);
      app.controller('byChCtrl', function ($scope) {
      });
    }
  }

  upElectric_bicycles() {
    this.Electric_bicycles++;
    if (this.totalDays) {
      this.total_amount += 25 * this.totalDays;
    }
    var app = app.module('byChApp', []);
    app.controller('byChCtrl', function ($scope) {
    });
  }
  downElectric_bicycles() {
    if (this.Electric_bicycles > 0) {
      this.Electric_bicycles--;
      if (this.totalDays) {
        this.total_amount -= 25 * this.totalDays;
      }
      var app = app.module('byChApp', []);
      app.controller('byChCtrl', function ($scope) {
      });
    }
  }

  upMountain_bicycles() {
    this.Mountain_bicycles++;
    if (this.totalDays) {
      this.total_amount += 14 * this.totalDays;
    }
    var app = app.module('byChApp', []);
    app.controller('byChCtrl', function ($scope) {
    });
  }
  downMountain_bicycles() {
    if (this.Mountain_bicycles > 0) {
      this.Mountain_bicycles--;
      if (this.totalDays) {
        this.total_amount -= 14 * this.totalDays;
      }
      var app = app.module('byChApp', []);
      app.controller('byChCtrl', function ($scope) {
      });
    }
  }

  upHelmet() {
    this.Helmet++;
    var app = app.module('byChApp', []);
    app.controller('byChCtrl', function ($scope) {
    });
  }
  downHelmet() {
    if (this.Helmet > 0) {
      this.Helmet--;
      var app = app.module('byChApp', []);
      app.controller('byChCtrl', function ($scope) {
      });
    }
  }

  upGlasses() {
    this.Glasses++;
    var app = app.module('byChApp', []);
    app.controller('byChCtrl', function ($scope) {
    });

  }
  downGlasses() {
    if (this.Glasses > 0) {
      this.Glasses--;
      var app = app.module('byChApp', []);
      app.controller('byChCtrl', function ($scope) {
      });
    }
  }

  upProtectors() {
    this.Protectors++;
    var app = app.module('byChApp', []);
    app.controller('byChCtrl', function ($scope) {
    });
  }
  downProtectors() {
    if (this.Protectors > 0) {
      this.Protectors--;
      var app = app.module('byChApp', []);
      app.controller('byChCtrl', function ($scope) {
      });
    }
  }
  changeSubmit() {
    this.submit = true;
    this.onSubmit();
  }

}
